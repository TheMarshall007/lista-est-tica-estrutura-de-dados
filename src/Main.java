import elementList.Element;
import genre.Genre;
import person.Person;

public class Main {
    public static void main(String[] args) {
        Element<Person> e = new Element();

        Person Leonardo = new Person("Leonardo", 20, Genre.MALE);
        Person Sara = new Person("Sara", 23, Genre.FEMALE);

        System.out.println("Is empty: " + e.isEmpty());
        e.addElement(Leonardo);
        e.addElement(Sara);
        e.printElements();
        System.out.println("Is empty: " + e.isEmpty());
        System.out.println("Elements Amount: " + e.getElementAmount());
        System.out.println("Element Leonardo position: " + e.getElementPosition(Leonardo));
        System.out.println(e.getElementInList(1));
        System.out.println("Remove element");
        e.removeElementByPosition(1);
        e.removeElement(Sara);
        e.printElements();
        System.out.println("Is empty: " + e.isEmpty());
    }
}
