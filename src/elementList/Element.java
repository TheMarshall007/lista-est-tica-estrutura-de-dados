package elementList;

import java.util.ArrayList;

public class Element<T> {
    private ArrayList<T> elements;
    private int elementAmount;

    public Element() {
        elements = new ArrayList<>();
        elementAmount = 0;
    }

    public boolean isEmpty() {
        return elementAmount == 0;
    }

    public int getElementAmount() {
        return elementAmount;
    }

    public boolean verifyElementInList(T element) {
            return elements.contains(element);
    }

    public boolean verifyPositionIsValid(int position) {
        return position <= elementAmount && position >= 0;
    }

    public T getElementInList(int position) {
        position--;
        if (verifyPositionIsValid(position)) {
            return elements.get(position);
        }
        return null;
    }

    public int getElementPosition(T element) {
        if (verifyElementInList(element)) {
            return elements.indexOf(element)+1;
        }
        return -1;
    }

    public void addElement(T element) {
        elements.add(element);
        elementAmount++;
    }

    public void removeElement(T element) {
        if (verifyElementInList(element)) {
            elements.remove(element);
            elementAmount--;
        }
    }

    public void removeElementByPosition(int position) {
        position--;
        if (verifyPositionIsValid(position)) {
            elements.remove(position);
            elementAmount--;
        }
    }

    public void printElements() {
        for (int i = 0; i < elementAmount; i++) {
            System.out.println("Element " + (i+1) + ": " + elements.get(i));
        }
    }
}
